## HOW TO USE

1. Add %RESTART_PARAMS to namelist
2. add restart_vars to %RESTART_PARAMS. It's a list of where to assign extra fields ie if nvar=7, write 5,6,7
3. set nrestart = - output number (it has to be NEGATIVE)
4. set filetype='ramses'


## HOW IT WORKS

The restart is treated like a new start. The code makes the grid, converts all grid information to particle types and then deposits it back onto the newly distributed grid. This means that there is a NEW OUTPUT produced after the first step, equivalent to output_00001 for a new start. This means that the output frequency for a normal restart and an adjusted restart will be out of phase by 1 main timestep. It also starts counting from 1 again.


